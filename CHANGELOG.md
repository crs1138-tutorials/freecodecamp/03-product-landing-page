# Changelog
All notable changes to the **03 Product Landing Page** will be documented in this file. I'm using [semantic versioning](https://semver.org/) and the structure of this changelog is based on [keepachangelog.com](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]
This section keeps track of upcoming changes and gives you a notice about what features you can expect in future.

## [1.0.0] - 2019-05-15

### Adds

#### Content
* adds product page `<header>` element with `id="header`
* adds company logo image within `#header` element with `id="header-img"`
* adds navigation element `<nav>` with `id="nav-bar"`
* adds three clickable elements inside the `nav` element, each with the class `nav-link`
* adds functionality that when user clicks on a navigation `.nav-link`, it takes him to the corresponding part of the landing page.
* adds `<form>` element with `id="form"`
* within form there is an `<input>` field with `id="email"` where user can enter an email addres
* the email input field uses HTML5 validation to confirm that the entered text is an email address
* within the form there is a submit `<input>` with corresponding `id="submit"`.
* when user clicks the `#submit` element, the email is submitted to a static page (using this mock URL: https://www.freecodecamp.com/email-submit) that confirms the email address was entered (and that it posted successfully
* adds the responsive video element with `id="video"`
* adds basic scroll into view functionality

#### Layout
* the navbar is always at the top of the viewport
* product landing page is using at least one media query
* product landing page utilizes CSS flexbox at least once

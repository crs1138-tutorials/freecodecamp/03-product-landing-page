(function (params) {
  console.log(`Main script init`);
  const navlinks = document.querySelectorAll('.nav-link');
  // console.log(navlinks);

  // navlinks = [...navlinks];
  navlinks.forEach((navlink) => {
    navlink.addEventListener('click', function (eve) {
      eve.preventDefault();
      const scrollToElement = document.querySelector(this.hash);
      scrollToElement.scrollIntoView({behavior: 'smooth', block: 'center'});
    });
  });

  const video = document.getElementById('video');
  window.addEventListener('DOMContentLoaded', function(eve) {
    videoAdjustAR(video);
  });
  window.addEventListener('resize', function () {
    videoAdjustAR(video);
  })

  function videoAdjustAR(elem) {
    const {offsetWidth} = elem;
    elem.height = Math.floor(offsetWidth / 16 * 9);
  }

})();